<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'avatar', 'name', 'surname', 'email', 'password', 'school_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function school() {
        return $this->hasOne('App\School', 'id', 'school_id');
    }

    public function groups() {
        return $this->belongsToMany('App\Group', 'groups_users', 'user_id', 'group_id');
    }

    public function plans() {
        return $this->hasMany('App\Plan', 'teacher_id', 'id');
    }

    public function lectures() {
        return $this->hasMany('App\Lecture', 'user_id', 'id');
    }

    public function tests() {
        return $this->hasMany('App\Test', 'user_id', 'id');
    }
}
