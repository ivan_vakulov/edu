<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = [
        'question', 'answer_id'
    ];

    public function test() {
        return $this->belongsTo('App\Test', 'test_id', 'id');
    }

    public function answer() {
        return $this->hasOne('App\Answer', 'id', 'answer_id');
    }

    public function answers() {
        return $this->hasMany('App\Answer', 'question_id', 'id');
    }
}
