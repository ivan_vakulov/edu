<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable = [
        'number', 'description', 'school_id'
    ];

    public function school() {
        return $this->hasOne('App\School', 'id', 'school_id');
    }

    public function users() {
        return $this->belongsToMany('App\User', 'groups_users', 'group_id', 'user_id');
    }

    public function plans() {
        return $this->hasMany('App\Plan', 'group_id', 'id');
    }
}
