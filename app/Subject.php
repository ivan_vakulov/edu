<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $fillable = [
        'name', 'credits', 'hours', 'school_id'
    ];

    public function plans() {
        return $this->hasMany('App\Plan', 'subject_id', 'id');
    }
}
