<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $fillable = [
        'group_id', 'teacher_id', 'subject_id'
    ];

    public function teacher() {
        return $this->belongsTo('App\User', 'teacher_id', 'id');
    }

    public function group() {
        return $this->belongsTo('App\Group', 'group_id', 'id');
    }

    public function subject() {
        return $this->belongsTo('App\Subject', 'subject_id', 'id');
    }

    public function lectures() {
        return $this->hasMany('App\Lecture', 'plan_id', 'id');
    }

    public function tests() {
        return $this->hasMany('App\Test', 'plan_id', 'id');
    }
}
