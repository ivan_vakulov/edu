<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lecture extends Model
{
    protected $fillable = [
        'title', 'text', 'plan_id'
    ];

    public function teacher() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function plan() {
        return $this->belongsTo('App\Plan', 'plan_id', 'id');
    }
}
