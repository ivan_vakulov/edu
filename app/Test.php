<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    protected $fillable = [
        'title', 'plan_id'
    ];

    public function plan() {
        return $this->belongsTo('App\Plan', 'plan_id', 'id');
    }

    public function teacher() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function questions() {
        return $this->hasMany('App\Question', 'test_id', 'id');
    }
}
