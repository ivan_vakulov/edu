<?php

namespace App\Http\Controllers;

use App\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SubjectsController extends Controller
{
    public function getSubjectById(Request $request, $id) {
        return response()->json([
            'data' => Subject::where(['id' => $id, 'school_id' => Auth::user()->school_id])->first(),
            'status' => true,
        ], 200);
    }

    public function getSubjects(Request $request) {
        if (Auth::user()->hasRole('HeadTeacher')) {
            $subjects = Subject::where('school_id', Auth::user()->school_id)->paginate(25);
        } else {
            // $subjects = Auth::user()->subjects();
        }

        return response()->json([
            'data' => $subjects,
            'status' => true,
        ], 200);
    }

    public function createSubject(Request $request) {
        $this->validate($request, [
           'name' => 'required',
           'credits' => 'required|numeric',
           'hours' => 'required|numeric'
        ]);

        $subject = new Subject([
            'name' => $request->name,
            'school_id' => Auth::user()->school_id,
            'credits' => $request->credits,
            'hours' => $request->hours
        ]);
        $subject->save();

        return response()->json([
            'subject' => $subject,
            'status' => true
        ], 200);
    }

    public function updateSubject(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required',
            'credits' => 'required|numeric',
            'hours' => 'required|numeric'
        ]);

        $subject = Subject::where(['id' => $id, 'school_id' => Auth::user()->school_id])->first();
        $subject->name = $request->name;
        $subject->credits = $request->credits;
        $subject->hours = $request->hours;
        $subject->save();

        return response()->json([
            'subject' => $subject,
            'status' => true
        ], 200);
    }

    public function deleteSubject(Request $request, $id) {
        $subject = Subject::where(['id' => $id, 'school_id' => Auth::user()->school_id])->first();
        $subject->delete();

        return response()->json([
            'status' => true
        ], 200);
    }

    public function searchSubjects(Request $request) {
        $data = Subject::where('school_id', Auth::user()->school->id)->where('name', 'like', $request->q . '%')->get();

        return response()->json([
            'data' => $data,
            'status' => true
        ], 200);
    }
}
