<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Question;
use App\Test;
use App\UserMark;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TestsController extends Controller
{
    public function getTestById(Request $request, $id) {
        if (Auth::user()->hasRole('Teacher')) {
            $test = Test::with('questions.answers')->find($id);
        } else {
            $mark = UserMark::where(['user_id' => Auth::user()->id, 'test_id' => $id])->first();
            if (isset($mark)) {
                return response()->json([
                    'data' => [],
                    'status' => false,
                ], 200);
            }

            $test = Test::with('questions.answers')->find($id);
            foreach ($test['questions'] as $question) {
                $question['answer_id'] = null;
            }
        }

        return response()->json([
            'data' => $test,
            'status' => true,
        ], 200);
    }

    public function getTests(Request $request) {
        if (Auth::user()->hasRole('Teacher')) {
            $tests = Auth::user()->tests()->paginate(25);
        } else {
            $tests = Test::whereHas('plan.group.users', function ($query) {
                $query->where('user_id', Auth::user()->id);
            })->paginate(25);
        }

        return response()->json([
            'data' => $tests,
            'status' => true
        ], 200);
    }

    public function createTest(Request $request) {
        $test = new Test();
        $test->title = $request->title;
        $test->teacher()->associate(Auth::user());
        $test->save();

        foreach ($request->questions as $q) {
            $question = new Question();
            $question->question = $q['question'];
            $question->test()->associate($test);
            $question->save();

            foreach ($q['answers'] as $a) {
                $answer = new Answer();
                $answer->answer = $a['answer'];
                $answer->question()->associate($question);
                $answer->save();
                if ($a['id'] == $q['answer_id']) {
                    $question->answer_id = $answer->id;
                    $question->save();
                }
            }
        }

        return response()->json([
            'test' => Test::with('questions.answers')->find($test->id),
            'status' => true
        ], 200);
    }

    public function updateTest(Request $request, $id) {
        $test = Test::find($id);
        $test->title = $request->title;
        $test->teacher()->associate(Auth::user());
        $test->save();

        foreach ($request->questions as $q) {
            $question = Question::find($q['id']);
            $question->question = $q['question'];
            $question->test()->associate($test);
            $question->save();

            foreach ($q['answers'] as $a) {
                $answer = Answer::find($a['id']);
                $answer->answer = $a['answer'];
                $answer->question()->associate($question);
                $answer->save();
                if ($a['id'] == $q['answer_id']) {
                    $question->answer_id = $answer->id;
                    $question->save();
                }
            }
        }

        return response()->json([
            'test' => Test::with('questions.answers')->find($test->id),
            'status' => true
        ], 200);
    }

    public function deleteTest(Request $request, $id) {
        $test = Test::find($id);
        $questions = $test->questions()->get();
        foreach ($questions as $question) {
            $question->answers()->delete();
        }
        $test->questions()->delete();
        $test->delete();

        return response()->json([
            'status' => true
        ], 200);
    }

    public function searchTests(Request $request) {
        $data = Test::where('user_id', Auth::user()->id)->where('title', 'like', $request->q . '%')->get();

        return response()->json([
            'data' => $data,
            'status' => true
        ], 200);
    }

    public function passTest(Request $request, $id) {
        $count = 0;
        foreach ($request->questions as $question) {
            $correct = Question::find($question['id'])->answer_id;
            if ($question['answer_id'] == $correct) {
                $count++;
            }
        }
        $mark = new UserMark();
        $mark->mark = $count;
        $mark->student()->associate(Auth::user());
        $mark->test()->associate(Test::find($id));
        $mark->save();

        return response()->json([
            'mark' => $mark,
            'status' => true
        ], 200);
    }
}
