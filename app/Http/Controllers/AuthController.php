<?php

namespace App\Http\Controllers;

use App\Mail\WelcomeMail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{

    public function signup(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'surname' => 'required',
            'email' => 'required|email|unique:users',
            'role' => 'required|string'
        ]);

        $random_password = str_random(8);
        $hashed_random_password = Hash::make($random_password);

        $user = new User([
            'school_id' => Auth::user()->school_id,
            'name' => $request->input('name'),
            'surname' => $request->input('surname'),
            'email' => $request->input('email'),
            'password' => $hashed_random_password,
        ]);
        $user->save();

        $user->assignRole($request->role);

        Mail::to($request->email)->send(new WelcomeMail([
            'name' => $request->input('name'),
            'surname' => $request->input('surname'),
            'email' => $request->input('email'),
            'password' => $random_password
        ]));

        return response()->json([
            'message' => 'Successfully created user!',
            'status' => true
        ], 201);
    }

    public function signin(Request $request) {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $credentials = $request->only('email', 'password');
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json([
                    'error' => 'Invalid credentials',
                    'status' => false
                ], 401);
            }
        } catch (JWTException $e) {
            return response()->json([
                'error' => 'Could not to create a token!',
                'status' => false
            ], 500);
        }
        $user = Auth::user();
        $user['role'] =  Auth::user()->roles()->pluck('name')[0];
        return response()->json([
            'token' => $token,
            'user' => $user,
            'status' => true
        ], 200);
    }

    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

}
