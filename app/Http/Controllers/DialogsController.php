<?php

namespace App\Http\Controllers;

use App\Dialog;
use App\Events\MessageSent;
use App\Message;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DialogsController extends Controller
{
    public function getDialogs(Request $request) {
        return response()->json([
            'dialogs' => Dialog::where('first_user_id',  Auth::user()->id)->orWhere('second_user_id',  Auth::user()->id)->with('first_user')->with('second_user')->get(),
            'status' => true
        ], 200);
    }

    public function getDialogMessages(Request $request, $id) {
        return response()->json([
            'messages' => Dialog::with('first_user')->with('second_user')->with('messages.user')->where('id', $id)->first(),
            'status' => true
        ], 200);
    }

    public function sendMessageToDialog(Request $request, $id) {
        $dialog = Dialog::find($id);
        $message = new Message([
            'message' => $request->message,
        ]);
        $message->dialog()->associate($dialog);
        $message->user()->associate(Auth::user());
        $message->save();

        event(new MessageSent($message));

        return response()->json([
            'status' => true
        ], 200);
    }

    public function checkDialog(Request $request) {

        $dialog = Dialog::where(['first_user_id' => $request->user_id, 'second_user_id' => Auth::user()->id])->first();

        if (!isset($dialog)) {
            $dialog = Dialog::where(['first_user_id' => Auth::user()->id, 'second_user_id' => $request->user_id])->first();
        }

        if (!isset($dialog)) {
            $dialog = new Dialog();
            $dialog->first_user()->associate(Auth::user());
            $dialog->second_user()->associate(User::find($request->user_id));
            $dialog->save();
        }

        return response()->json([
            'dialog' => $dialog,
            'status' => true
        ], 200);
    }
}
