<?php

namespace App\Http\Controllers;

use App\Lecture;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LecturesController extends Controller
{
    public function getLectureById(Request $request, $id) {
        return response()->json([
            'data' => Lecture::where(['id' => $id])->first(),
            'status' => true,
        ], 200);
    }

    public function getLectures(Request $request) {
        if (Auth::user()->hasRole('Teacher')) {
            $lectures = Auth::user()->lectures()->paginate(25);
        } else {
            $lectures = Lecture::whereHas('plan.group.users', function ($query) {
                $query->where('user_id', Auth::user()->id);
            })->paginate(25);
        }

        return response()->json([
            'data' => $lectures,
            'status' => true
        ], 200);
    }

    public function createLecture(Request $request) {
        $this->validate($request, [
            'title' => 'required',
            'text' => 'required'
        ]);

        $lecture = new Lecture();
        $lecture->title = $request->title;
        $lecture->text = $request->text;
        $lecture->teacher()->associate(Auth::user());
        $lecture->save();

        return response()->json([
            'lecture' => $lecture,
            'status' => true
        ], 200);
    }

    public function updateLecture(Request $request, $id) {
        $this->validate($request, [
            'title' => 'required',
            'text' => 'required'
        ]);

        $lecture = Lecture::find($id);
        $lecture->title = $request->title;
        $lecture->text = $request->text;
        $lecture->save();

        return response()->json([
            'lecture' => $lecture,
            'status' => true
        ], 200);
    }

    public function deleteLecture(Request $request, $id) {
        $lecture = Lecture::find($id);
        $lecture->delete();

        return response()->json([
            'status' => true
        ], 200);
    }

    public function searchLectures(Request $request) {
        $data = Lecture::where('user_id', Auth::user()->id)->where('title', 'like', $request->q . '%')->get();

        return response()->json([
            'data' => $data,
            'status' => true
        ], 200);
    }
}
