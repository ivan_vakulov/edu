<?php

namespace App\Http\Controllers;

use App\School;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SchoolsController extends Controller
{
    public function getSchool(Request $request) {
        return response()->json([
            'school' => School::find(Auth::user()->school->id),
            'status' => true
        ], 200);
    }

    public function updateSchool(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'city' => 'required',
            'phone' => 'required',
            'address' => 'required',
        ]);

        $school = School::find(Auth::user()->school->id);
        $school->name = $request->name;
        $school->phone = $request->phone;
        $school->city = $request->city;
        $school->address = $request->address;
        $school->save();

        return response()->json([
            'school' => $school,
            'status' => true
        ], 200);
    }
}
