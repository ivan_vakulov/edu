<?php

namespace App\Http\Controllers;

use App\Group;
use App\Lecture;
use App\Plan;
use App\Subject;
use App\Test;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PlansController extends Controller
{
    public function getPlanById(Request $request, $id) {
        return response()->json([
            'data' => Plan::where(['id' => $id, 'school_id' => Auth::user()->school->id])->with('group')->with('teacher')->with('subject')->first(),
            'status' => true,
        ], 200);
    }

    public function getPlans(Request $request) {
        if (Auth::user()->hasRole('HeadTeacher')) {
            $plans = Plan::where('school_id', Auth::user()->school_id)->with('group')->with('teacher')->with('subject')->paginate(25);
        } else {
            $plans = Auth::user()->plans()->with('group')->with('teacher')->with('subject')->paginate(25);
        }

        return response()->json([
            'data' => $plans,
            'status' => true
        ], 200);
    }

    public function createPlan(Request $request) {
        $plan = Plan::where(['group_id' => $request->group['id'], 'teacher_id' => $request->user['id'], 'subject_id' => $request->subject['id']])->first();

        if (!isset($plan)) {
            $plan = new Plan();
            $plan->school_id = Auth::user()->school_id;
            $plan->subject()->associate(Subject::find($request->subject['id']));
            $plan->teacher()->associate(User::find($request->user['id']));
            $plan->group()->associate(Group::find($request->group['id']));
            $plan->save();
        }

        return response()->json([
            'plan' => $plan,
            'status' => true
        ], 200);
    }

    public function updatePlan(Request $request, $id) {
        $this->validate($request, [
            'user' => 'required',
            'group' => 'required',
            'subject' => 'required'
        ]);

        $plan = Plan::where(['id' => $id, 'school_id' => Auth::user()->school->id])->first();
        $plan->subject()->associate(Subject::find($request->subject['id']));
        $plan->teacher()->associate(User::find($request->user['id']));
        $plan->group()->associate(Group::find($request->group['id']));
        $plan->save();

        return response()->json([
            'group' => $plan,
            'status' => true
        ], 200);
    }

    public function deletePlan(Request $request, $id) {
        $plan = Plan::where(['id' => $id, 'school_id' => Auth::user()->school->id])->first();
        $plan->delete();

        return response()->json([
            'status' => true
        ], 200);
    }

    public function getPlanLectures(Request $request, $id) {
        return response()->json([
            'data' => Plan::find($id)->lectures()->paginate(25),
            'status' => true
        ], 200);
    }

    public function getPlanTests(Request $request, $id) {
        return response()->json([
            'data' => Plan::find($id)->tests()->paginate(25),
            'status' => true
        ], 200);
    }

    public function addLectureToPlan(Request $request, $id) {
        $this->validate($request, [
            'testId' => 'required'
        ]);

        Plan::find($id)->lectures()->save(Lecture::find($request->lectureId));

        return response()->json([
            'status' => true
        ], 200);
    }

    public function addTestToPlan(Request $request, $id) {
        $this->validate($request, [
            'testId' => 'required'
        ]);

        Plan::find($id)->tests()->save(Test::find($request->testId));

        return response()->json([
            'status' => true
        ], 200);
    }

    public function deleteLectureFromPlan(Request $request, $id) {
        $this->validate($request, [
            'lectureId' => 'required'
        ]);

        $lecture = Lecture::find($request->lectureId);
        $lecture->plan_id = null;
        $lecture->save();

        return response()->json([
            'status' => true
        ], 200);
    }

    public function deleteTestFromPlan(Request $request, $id) {
        $this->validate($request, [
            'testId' => 'required'
        ]);

        $test = Test::find($request->testId);
        $test->plan_id = null;
        $test->save();

        return response()->json([
            'status' => true
        ], 200);
    }
}
