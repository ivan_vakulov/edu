<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function getAuthUser(Request $request) {
        $user = Auth::user();
        $user['role'] =  Auth::user()->roles()->pluck('name')[0];

        return response()->json([
            'user' => $user,
            'status' => true
        ], 200);
    }

    public function getUserById(Request $request, $id) {
        return response()->json([
            'user' => User::where(['school_id' => Auth::user()->school_id, 'id' => $id])->first(),
            'status' => true
        ], 200);
    }

    public function update(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required',
            'surname' => 'required'
        ]);

        $user = User::where(['school_id' => Auth::user()->school_id, 'id' => $id])->first();
        $user->name = $request->name;
        $user->surname = $request->surname;
        $user->save();

        return response()->json([
            'status' => true
        ], 200);
    }

    public function updateAuthUser(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'surname' => 'required',
            'password' => 'nullable',
            'password_new' => 'nullable'
        ]);

        $user = Auth::user();
        $user->name = $request->name;
        $user->surname = $request->surname;

        if (isset($request->password) && isset($request->password_new)) {
            if (!Hash::check($request->password, $user->password)) {
                return response()->json([
                    'error' => 'Invalid credentials',
                    'status' => false
                ], 422);
            }
            $user->password = Hash::make($request->password_new);
        }

        $user->save();

        $user = Auth::user();
        $user['role'] =  Auth::user()->roles()->pluck('name')[0];

        return response()->json([
            'user' => $user,
            'status' => true
        ], 200);
    }

    public function updateAvatar(Request $request, $id) {
        $this->validate($request, [
            'avatar' => 'image'
        ]);

        $user = User::find($id);
        if (isset($user->avatar)) {
            Storage::delete($user->avatar);
        }
        $path = $request->avatar->store('/public/images');
        $user->avatar = Storage::url($path);
        $user->save();

        return response()->json([
            'url' => $user->avatar,
            'status' => true
        ], 200);
    }

    public function delete(Request $request, $id) {
        $user = User::where(['school_id' => Auth::user()->school_id, 'id' => $id])->first();
        $user->delete();

        return response()->json([
            'status' => true
        ], 200);
    }

    public function getUsers(Request $request) {
        if ($request->role) {
            $data = User::role($request->role)->where('school_id', Auth::user()->school_id)->paginate(25);
        } else {
            $data = User::where('school_id', Auth::user()->school_id)->paginate(25);
        }

        return response()->json([
            'data' => $data,
            'status' => true
        ], 200);
    }

    public function searchUsers(Request $request) {
        if ($request->role) {
            $data = User::role($request->role)->where('school_id', Auth::user()->school->id)->where('name', 'like', $request->q . '%')->orWhere('surname', 'like', $request->q . '%')->get();
        } else {
            $data = User::where('school_id', Auth::user()->school->id)->where('name', 'like', $request->q . '%')->orWhere('surname', 'like', $request->q . '%')->get();
        }

        return response()->json([
            'data' => $data,
            'status' => true
        ], 200);
    }
}
