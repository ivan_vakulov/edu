<?php

namespace App\Http\Controllers;

use App\Group;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GroupsController extends Controller
{
    public function getGroupById(Request $request, $id) {
        return response()->json([
            'data' => Group::where(['id' => $id, 'school_id' => Auth::user()->school->id])->first(),
            'status' => true,
        ], 200);
    }

    public function getGroups(Request $request) {
        if (Auth::user()->hasRole('HeadTeacher')) {
            $groups = Group::where('school_id', Auth::user()->school->id)->paginate(25);
        } else {
            $groups = Auth::user()->groups();
        }

        return response()->json([
            'data' => $groups,
            'status' => true,
        ], 200);
    }

    public function createGroup(Request $request) {
        $this->validate($request, [
            'number' => 'required',
            'description' => 'required'
        ]);

        $group = new Group([
            'number' => $request->number,
            'school_id' => Auth::user()->school->id,
            'description' => $request->description
        ]);
        $group->save();

        return response()->json([
            'group' => $group,
            'status' => true
        ], 200);
    }

    public function updateGroup(Request $request, $id) {
        $this->validate($request, [
            'number' => 'required',
            'description' => 'required'
        ]);

        $group = Group::where(['id' => $id, 'school_id' => Auth::user()->school->id])->first();
        $group->number = $request->number;
        $group->description = $request->description;
        $group->save();

        return response()->json([
            'group' => $group,
            'status' => true
        ], 200);
    }

    public function deleteGroup(Request $request, $id) {
        $group = Group::where(['id' => $id, 'school_id' => Auth::user()->school->id])->first();
        $group->delete();

        return response()->json([
            'status' => true
        ], 200);
    }

    public function getGroupStudents(Request $request, $id) {
        return response()->json([
            'data' => Group::where(['id' => $id, 'school_id' => Auth::user()->school->id])->first()->users()->paginate(25),
            'status' => true,
        ], 200);
    }

    public function deleteStudentFromGroup(Request $request, $id) {
        $this->validate($request, [
            'studentId' => 'required'
        ]);

        Group::where(['id' => $id, 'school_id' => Auth::user()->school->id])->first()->users()->detach($request->studentId);

        return response()->json([
            'status' => true
        ], 200);
    }

    public function addStudentToGroup(Request $request, $id) {
        $this->validate($request, [
            'studentId' => 'required'
        ]);

        Group::where(['id' => $id, 'school_id' => Auth::user()->school->id])->first()->users()->sync($request->studentId);

        return response()->json([
            'status' => true
        ], 200);
    }

    public function searchGroups(Request $request) {
        $data = Group::where('school_id', Auth::user()->school->id)->where('number', 'like', $request->q . '%')->get();

        return response()->json([
            'data' => $data,
            'status' => true
        ], 200);
    }
}
