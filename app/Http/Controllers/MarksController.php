<?php

namespace App\Http\Controllers;

use App\UserMark;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MarksController extends Controller
{
    public function getMarks(Request $request) {
        if (Auth::user()->hasRole('Teacher')) {
            $data = UserMark::with('test.plan.subject')->with('student')->whereHas('test.plan', function ($query) {
                $query->where('teacher_id', Auth::user()->id);
            })->paginate(25);
        } else {
            $data = UserMark::with('test.plan.subject')->where('user_id', Auth::user()->id)->paginate(25);
        }

        return response()->json([
            'data' => $data,
            'status' => true
        ], 200);
    }
}
