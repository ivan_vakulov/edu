<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMark extends Model
{
    protected $fillable = [
        'mark'
    ];

    public function student() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function test() {
        return $this->belongsTo('App\Test', 'test_id', 'id');
    }
}
