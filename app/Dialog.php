<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dialog extends Model
{
    public function messages() {
        return $this->hasMany('App\Message', 'dialog_id', 'id');
    }

    public function first_user() {
        return $this->belongsTo('App\User', 'first_user_id', 'id');
    }

    public function second_user() {
        return $this->belongsTo('App\User', 'second_user_id', 'id');
    }
}
