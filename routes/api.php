<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/user/signin', 'AuthController@signin');

Route::group(['middleware' => ['auth.jwt']], function() {
    Route::post('/user', 'AuthController@signup')->middleware('permission:create users');
    Route::post('/user/logout', 'AuthController@logout');

    Route::get('/users', 'UserController@getUsers')->middleware('permission:read users');
    Route::get('/users/search', 'UserController@searchUsers')->middleware('permission:read users');
    Route::get('/users/me', 'UserController@getAuthUser');
    Route::put('/users/me', 'UserController@updateAuthUser');
    Route::get('/users/{id}', 'UserController@getUserById')->middleware('permission:read users');
    Route::put('/users/{id}', 'UserController@update')->middleware('permission:update users');
    Route::post('/users/{id}/avatar', 'UserController@updateAvatar');
    Route::delete('/users/{id}', 'UserController@delete')->middleware('permission:delete users');

    Route::get('/school', 'SchoolsController@getSchool')->middleware('permission:read school');
    Route::put('/school', 'SchoolsController@updateSchool')->middleware('permission:update school');

    Route::get('/subjects', 'SubjectsController@getSubjects')->middleware('permission:read subjects');
    Route::get('/subjects/search', 'SubjectsController@searchSubjects')->middleware('permission:read subjects');
    Route::post('/subjects', 'SubjectsController@createSubject')->middleware('permission:create subjects');
    Route::get('/subjects/{id}', 'SubjectsController@getSubjectById')->middleware('permission:read subjects');
    Route::put('/subjects/{id}', 'SubjectsController@updateSubject')->middleware('permission:update subjects');
    Route::delete('/subjects/{id}', 'SubjectsController@deleteSubject')->middleware('permission:delete subjects');

    Route::get('/groups', 'GroupsController@getGroups')->middleware('permission:read groups');
    Route::get('/groups/search', 'GroupsController@searchGroups')->middleware('permission:read groups');
    Route::post('/groups', 'GroupsController@createGroup')->middleware('permission:create groups');
    Route::get('/groups/{id}', 'GroupsController@getGroupById')->middleware('permission:read groups');
    Route::put('/groups/{id}', 'GroupsController@updateGroup')->middleware('permission:update groups');
    Route::delete('/groups/{id}', 'GroupsController@deleteGroup')->middleware('permission:delete groups');
    Route::get('/groups/{id}/students', 'GroupsController@getGroupStudents')->middleware('permission:update groups');
    Route::put('/groups/{id}/students', 'GroupsController@deleteStudentFromGroup')->middleware('permission:update groups');
    Route::post('/groups/{id}/students', 'GroupsController@addStudentToGroup')->middleware('permission:update groups');

    Route::get('/plans', 'PlansController@getPlans')->middleware('permission:read plans');
    Route::post('/plans', 'PlansController@createPlan')->middleware('permission:create plans');
    Route::get('/plans/{id}', 'PlansController@getPlanById')->middleware('permission:read plans');
    Route::put('/plans/{id}', 'PlansController@updatePlan')->middleware('permission:update plans');
    Route::delete('/plans/{id}', 'PlansController@deletePlan')->middleware('permission:delete plans');

    Route::get('/plans/{id}/lectures', 'PlansController@getPlanLectures')->middleware('permission:read plans');
    Route::post('/plans/{id}/lectures', 'PlansController@addLectureToPlan')->middleware('permission:read plans');
    Route::put('/plans/{id}/lectures', 'PlansController@deleteLectureFromPlan')->middleware('permission:read plans');

    Route::get('/plans/{id}/tests', 'PlansController@getPlanTests')->middleware('permission:read plans');
    Route::post('/plans/{id}/tests', 'PlansController@addTestToPlan')->middleware('permission:read plans');
    Route::put('/plans/{id}/tests', 'PlansController@deleteTestFromPlan')->middleware('permission:read plans');

    Route::get('/lectures/search', 'LecturesController@searchLectures')->middleware('permission:read lectures');
    Route::get('/lectures', 'LecturesController@getLectures')->middleware('permission:read lectures');
    Route::post('/lectures', 'LecturesController@createLecture')->middleware('permission:create lectures');
    Route::get('/lectures/{id}', 'LecturesController@getLectureById')->middleware('permission:read lectures');
    Route::put('/lectures/{id}', 'LecturesController@updateLecture')->middleware('permission:update lectures');
    Route::delete('/lectures/{id}', 'LecturesController@deleteLecture')->middleware('permission:delete lectures');

    Route::get('/tests/search', 'TestsController@searchTests')->middleware('permission:read tests');
    Route::get('/tests', 'TestsController@getTests')->middleware('permission:read tests');
    Route::post('/tests', 'TestsController@createTest')->middleware('permission:create tests');
    Route::get('/tests/{id}', 'TestsController@getTestById')->middleware('permission:read tests');
    Route::put('/tests/{id}', 'TestsController@updateTest')->middleware('permission:update tests');
    Route::delete('/tests/{id}', 'TestsController@deleteTest')->middleware('permission:delete tests');
    Route::post('/tests/{id}/pass', 'TestsController@passTest')->middleware('permission:read tests');

    Route::get('/dialogs', 'DialogsController@getDialogs');
    Route::get('/dialogs/{id}', 'DialogsController@getDialogMessages');
    Route::post('/dialogs/{id}', 'DialogsController@sendMessageToDialog');
    Route::post('/dialog', 'DialogsController@checkDialog');

    Route::get('/marks', 'MarksController@getMarks')->middleware('permission:read tests');;

});