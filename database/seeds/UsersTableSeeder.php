<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User();
        $user->name = 'Ivan';
        $user->school_id = '1';
        $user->surname = 'Director';
        $user->email = 'director@gmail.com';
        $user->password = bcrypt('asdasd');
        $user->save();

        $user->assignRole('Director');

        $user = new \App\User();
        $user->name = 'Ivan';
        $user->school_id = '1';
        $user->surname = 'Head teacher';
        $user->email = 'headteacher@gmail.com';
        $user->password = bcrypt('asdasd');
        $user->save();

        $user->assignRole('HeadTeacher');

        $user = new \App\User();
        $user->name = 'Ivan';
        $user->school_id = '1';
        $user->surname = 'Teacher';
        $user->email = 'teacher@gmail.com';
        $user->password = bcrypt('asdasd');
        $user->save();

        $user->assignRole('Teacher');

        $user = new \App\User();
        $user->name = 'Ivan';
        $user->school_id = '1';
        $user->surname = 'Student';
        $user->email = 'student@gmail.com';
        $user->password = bcrypt('asdasd');
        $user->save();

        $user->assignRole('Student');

        $group = new \App\Group();
        $group->school_id = '1';
        $group->number = '123';
        $group->description = 'Group description';
        $group->save();
        $group->users()->attach($user->id);

        $dialog = new \App\Dialog();
        $dialog->first_user_id = '1';
        $dialog->second_user_id = '2';
        $dialog->save();

        $message = new \App\Message();
        $message->message = 'Hello 2';
        $message->user()->associate(\App\User::find('1'));
        $message->dialog()->associate($dialog);
        $message->save();

        $message = new \App\Message();
        $message->message = 'Hello 1';
        $message->user()->associate(\App\User::find('2'));
        $message->dialog()->associate($dialog);
        $message->save();
    }
}
