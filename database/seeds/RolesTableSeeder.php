<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create(['name' => 'create users']);
        Permission::create(['name' => 'read users']);
        Permission::create(['name' => 'update users']);
        Permission::create(['name' => 'delete users']);

        Permission::create(['name' => 'read school']);
        Permission::create(['name' => 'update school']);

        Permission::create(['name' => 'create subjects']);
        Permission::create(['name' => 'read subjects']);
        Permission::create(['name' => 'update subjects']);
        Permission::create(['name' => 'delete subjects']);

        Permission::create(['name' => 'create groups']);
        Permission::create(['name' => 'read groups']);
        Permission::create(['name' => 'update groups']);
        Permission::create(['name' => 'delete groups']);

        Permission::create(['name' => 'create plans']);
        Permission::create(['name' => 'read plans']);
        Permission::create(['name' => 'update plans']);
        Permission::create(['name' => 'delete plans']);

        Permission::create(['name' => 'create lectures']);
        Permission::create(['name' => 'read lectures']);
        Permission::create(['name' => 'update lectures']);
        Permission::create(['name' => 'delete lectures']);

        Permission::create(['name' => 'create tests']);
        Permission::create(['name' => 'read tests']);
        Permission::create(['name' => 'update tests']);
        Permission::create(['name' => 'delete tests']);

        $director = Role::create(['name' => 'Director']);
        $director->givePermissionTo('create users');
        $director->givePermissionTo('read users');
        $director->givePermissionTo('update users');
        $director->givePermissionTo('delete users');
        $director->givePermissionTo('read school');
        $director->givePermissionTo('update school');

        $head_teacher = Role::create(['name' => 'HeadTeacher']);
        $head_teacher->givePermissionTo('create users');
        $head_teacher->givePermissionTo('read users');
        $head_teacher->givePermissionTo('update users');
        $head_teacher->givePermissionTo('delete users');

        $head_teacher->givePermissionTo('create subjects');
        $head_teacher->givePermissionTo('read subjects');
        $head_teacher->givePermissionTo('update subjects');
        $head_teacher->givePermissionTo('delete subjects');

        $head_teacher->givePermissionTo('create groups');
        $head_teacher->givePermissionTo('read groups');
        $head_teacher->givePermissionTo('update groups');
        $head_teacher->givePermissionTo('delete groups');

        $head_teacher->givePermissionTo('create plans');
        $head_teacher->givePermissionTo('read plans');
        $head_teacher->givePermissionTo('update plans');
        $head_teacher->givePermissionTo('delete plans');

        $teacher = Role::create(['name' => 'Teacher']);
        $teacher->givePermissionTo('read users');
        $teacher->givePermissionTo('update users');
        $teacher->givePermissionTo('read subjects');
        $teacher->givePermissionTo('read groups');

        $teacher->givePermissionTo('read plans');

        $teacher->givePermissionTo('create lectures');
        $teacher->givePermissionTo('read lectures');
        $teacher->givePermissionTo('update lectures');
        $teacher->givePermissionTo('delete lectures');

        $teacher->givePermissionTo('create tests');
        $teacher->givePermissionTo('read tests');
        $teacher->givePermissionTo('update tests');
        $teacher->givePermissionTo('delete tests');

        $student = Role::create(['name' => 'Student']);
        $student->givePermissionTo('read users');
        $student->givePermissionTo('read subjects');
        $student->givePermissionTo('read groups');
        $student->givePermissionTo('read lectures');
        $student->givePermissionTo('read tests');

    }
}
