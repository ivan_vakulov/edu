<?php

use Illuminate\Database\Seeder;

class PlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User();
        $user->name = 'Sasha';
        $user->school_id = '1';
        $user->surname = 'Test';
        $user->email = 'sashateacher@gmail.com';
        $user->password = bcrypt('asdasd');
        $user->save();

        $user->assignRole('Teacher');

        $group = new \App\Group();
        $group->school_id = '1';
        $group->number = '123123123';
        $group->description = 'Group description 123123123';
        $group->save();

        $subject = new \App\Subject([
            'name' => 'БД',
            'school_id' => '1',
            'credits' => 3.5,
            'hours' => 4
        ]);
        $subject->save();

        $plan = new \App\Plan();
        $plan->school_id = '1';
        $plan->group()->associate($group);
        $plan->teacher()->associate($user);
        $plan->subject()->associate($subject);
        $plan->save();

        $lection = new \App\Lecture();
        $lection->teacher()->associate($user);
        $lection->plan()->associate($plan);
        $lection->title = 'Title';
        $lection->text = 'Text';
        $lection->save();

        $test = new \App\Test();
        $test->title = 'First test';

        $question = new \App\Question();
        $question->question = 'First question';

        $answer1 = new \App\Answer();
        $answer1->answer = 'First answer';
        $answer1->save();

        $answer2 = new \App\Answer();
        $answer2->answer = 'Second answer';
        $answer2->save();

        $test->plan()->associate($plan);
        $test->teacher()->associate($user);
        $test->save();

        $question->test()->associate($test);
        $question->answer_id = $answer1->id;
        $question->save();

        $answer1->question()->associate($question);
        $answer1->save();

        $answer2->question()->associate($question);
        $answer2->save();

    }
}
