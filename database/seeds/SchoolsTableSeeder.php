<?php

use Illuminate\Database\Seeder;

class SchoolsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $school = new \App\School();
        $school->name = 'ЗОШ №327';
        $school->city = 'Київ';
        $school->phone = '0444265325';
        $school->address = 'Малиновського 27Б';
        $school->save();
    }
}
