<?php

use Illuminate\Database\Seeder;

class SubjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subject = new \App\Subject([
            'name' => 'МПА',
            'school_id' => '1',
            'credits' => 3.5,
            'hours' => 4
        ]);
        $subject->save();

        $subject = new \App\Subject([
            'name' => 'МЛТА',
            'school_id' => '1',
            'credits' => 3,
            'hours' => 4
        ]);
        $subject->save();

        $subject = new \App\Subject([
            'name' => 'ЛААГ',
            'school_id' => '1',
            'credits' => 3.5,
            'hours' => 3.5
        ]);
        $subject->save();

        $subject = new \App\Subject([
            'name' => 'АСД',
            'school_id' => '1',
            'credits' => 4.5,
            'hours' => 3
        ]);
        $subject->save();
    }
}
