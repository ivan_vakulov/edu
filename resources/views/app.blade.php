<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <link rel="shortcut icon" href="/favicon.ico">
        <title>EDU project</title>

        <link rel="stylesheet" href="/css/app.css">

    </head>
    <body class="app">
        <div id="app">
            <transition name="fade" mode="out-in">
                <router-view></router-view>
            </transition>
        </div>
        <script src="/js/app.js"></script>
    </body>
</html>
