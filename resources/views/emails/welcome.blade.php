<!DOCTYPE html>
<html>
    <head>
        <title>Welcome Email</title>
    </head>

    <body>
        <h2>Welcome to EDU, {{ $info['name'] }} {{ $info['surname'] }}!</h2>
        <br/>
        Your registered login is {{$info['email']}}.
        <br/>
        Your registered password is {{$info['password']}}, You can change it in your settings.
    </body>

</html>