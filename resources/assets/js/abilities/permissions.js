import { AbilityBuilder } from '@casl/ability'

export default function(type) {
    return AbilityBuilder.define(can => {
        switch(type) {
            case 'Director':
                can(['read', 'update'], 'Profile');
                can(['create', 'read', 'update', 'delete'], 'HeadTeachers');
                can(['create', 'read', 'update', 'delete'], 'Teachers');
                can(['create', 'read', 'update', 'delete'], 'Students');
                can(['read', 'update'], 'School');
                can(['read'], 'Statistic');
                break;
            case 'HeadTeacher':
                can(['read', 'update'], 'Profile');
                can(['read'], 'HeadTeachers');
                can(['create', 'read', 'update', 'delete'], 'Teachers');
                can(['create', 'read', 'update', 'delete'], 'Groups');
                can(['create', 'read', 'update', 'delete'], 'Students');
                can(['create', 'read', 'update', 'delete'], 'Subjects');
                can(['create', 'read', 'update', 'delete'], 'Plans');
                can(['read'], 'Statistic');
                break;
            case 'Teacher':
                can(['read', 'update'], 'Profile');
                can(['read', 'update'], 'Groups');
                can(['create', 'read'], 'Students');
                can(['read'], 'Plans');
                can(['update'], 'PlansProgram');
                can(['create', 'read', 'update', 'delete'], 'Lectures');
                can(['create', 'read', 'update', 'delete'], 'Tests');
                can(['read'], 'Statistic');
                break;
            case 'Student':
                can(['read', 'update'], 'Profile');
                can(['read'], 'Lectures');
                can(['read', 'pass'], 'Tests');
                can(['read'], 'Statistic');
                break;
        }
    })
};