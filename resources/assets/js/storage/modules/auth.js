/**
 * Mutations
 */
const AUTHENTICATE = 'AUTHENTICATE';
const CLEAR = 'CLEAR';
const SET_USER = 'SET_USER';

const state = {
    accessToken: localStorage.getItem('accessToken') || '',
    user: JSON.parse(localStorage.getItem('user')) || {}
};

const getters = {
    getTokenHeader: state => 'Bearer ' + state.accessToken,
    getIsAuthorized: state => !!state.accessToken
};

const mutations = {
    [AUTHENTICATE] (state, token) {
        state.accessToken = token;
        localStorage.setItem('accessToken', token);
    },
    [CLEAR] (state) {
        state.accessToken = '';
        state.user = {};
        localStorage.removeItem('accessToken');
        localStorage.removeItem('user');
    },
    [SET_USER] (state, payload) {
        state.user = payload;
        localStorage.setItem('user', JSON.stringify(payload));
    }
};

const actions = {
    login ({commit, getters}, payload) {
        commit(AUTHENTICATE, payload.token);
        commit(SET_USER, payload.user);
    },
    logout (context) {
        context.commit(CLEAR);
    },
    updateUser (context, payload) {
        context.commit(SET_USER, payload);
    }
};

/**
 * Export
 */
export default {
    state,
    getters,
    actions,
    mutations
}