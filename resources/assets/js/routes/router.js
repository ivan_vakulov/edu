import Vue from 'vue';
import VueRouter from 'vue-router';
import store from '../storage/storage';
import defineAbilitiesFor from '../abilities/permissions';

Vue.use(VueRouter);

const LogIn = () => System.import('../components/auth/LogIn.vue');
const Profile = () => System.import('../components/pages/profile/Profile.vue');
const MainLayout = () => System.import('../components/layout/MainLayout.vue');
const Dashboard = () => System.import('../components/pages/panel/Dashboard.vue');
const Students = () => System.import('../components/pages/panel/users/Students.vue');
const Subjects = () => System.import('../components/pages/panel/subjects/Subjects.vue');
const SubjectEdit = () => System.import('../components/pages/panel/subjects/SubjectEdit.vue');
const Lectures = () => System.import('../components/pages/panel/lectures/Lectures.vue');
const LecturesEdit = () => System.import('../components/pages/panel/lectures/LecturesEdit.vue');
const Tests = () => System.import('../components/pages/panel/tests/Tests.vue');
const TestsEdit = () => System.import('../components/pages/panel/tests/TestsEdit.vue');
const TestsPass = () => System.import('../components/pages/panel/tests/TestsPass.vue');
const Plans = () => System.import('../components/pages/panel/plans/Plans.vue');
const PlansEdit = () => System.import('../components/pages/panel/plans/PlansEdit.vue');
const PlansProgram = () => System.import('../components/pages/panel/plans/PlansProgram.vue');
const Groups = () => System.import('../components/pages/panel/groups/Groups.vue');
const GroupsEdit = () => System.import('../components/pages/panel/groups/GroupsEdit.vue');
const HeadTeachers = () => System.import('../components/pages/panel/users/HeadTeachers.vue');
const Teachers = () => System.import('../components/pages/panel/users/Teachers.vue');
const UserEdit = () => System.import('../components/pages/panel/users/UserEdit.vue');
const School = () => System.import('../components/pages/panel/School.vue');
const Statistic = () => System.import('../components/pages/panel/Statistic.vue');
const Chat = () => System.import('../components/pages/panel/chat/Chat.vue');
const ChatMessages = () => System.import('../components/pages/panel/chat/ChatMessages.vue');
const Marks = () => System.import('../components/pages/panel/Marks.vue');
const NotFound = () => System.import('../components/pages/static/NotFound.vue');
const NotPermitted = () => System.import('../components/pages/static/NotPermitted.vue');

const routes = [
    {
        path: '/auth',
        component: LogIn,
        beforeEnter(to, from, next) {
            if (store.getters.getIsAuthorized) {
                next({ path: '/' })
            } else {
                next()
            }
        }
    },
    {
        path: '/',
        component: MainLayout,
        children: [
            { path: '/', component: Dashboard },
            {
                path: '/profile', component: Profile,
                beforeEnter(to, from, next) {
                    defineAbilitiesFor(store.state.auth.user.role).can('update', 'Profile') ? next() : next('/not-permitted')
                }
            },

            {
                path: '/school', component: School,
                beforeEnter(to, from, next) {
                    defineAbilitiesFor(store.state.auth.user.role).can('update', 'School') ? next() : next('/not-permitted')
                }
            },

            {
                path: '/head-teachers', component: HeadTeachers,
                beforeEnter(to, from, next) {
                    defineAbilitiesFor(store.state.auth.user.role).can('read', 'HeadTeachers') ? next() : next('/not-permitted')
                }
            },
            {
                path: '/head-teachers/create', component: UserEdit, props: { role: 'HeadTeacher' },
                beforeEnter(to, from, next) {
                    defineAbilitiesFor(store.state.auth.user.role).can('create', 'HeadTeachers') ? next() : next('/not-permitted')
                }
            },
            {
                path: '/head-teachers/:id', component: UserEdit, props: { role: 'HeadTeacher' },
                beforeEnter(to, from, next) {
                    defineAbilitiesFor(store.state.auth.user.role).can('update', 'HeadTeachers') ? next() : next('/not-permitted')
                }
            },

            {
                path: '/teachers', component: Teachers,
                beforeEnter(to, from, next) {
                    defineAbilitiesFor(store.state.auth.user.role).can('read', 'Teachers') ? next() : next('/not-permitted')
                }
            },
            {
                path: '/teachers/create', component: UserEdit, props: { role: 'Teacher' },
                beforeEnter(to, from, next) {
                    defineAbilitiesFor(store.state.auth.user.role).can('create', 'Teachers') ? next() : next('/not-permitted')
                }
            },
            {
                path: '/teachers/:id', component: UserEdit, props: { role: 'Teacher' },
                beforeEnter(to, from, next) {
                    defineAbilitiesFor(store.state.auth.user.role).can('update', 'Teachers') ? next() : next('/not-permitted')
                }
            },

            {
                path: '/students', component: Students,
                beforeEnter(to, from, next) {
                    defineAbilitiesFor(store.state.auth.user.role).can('read', 'Students') ? next() : next('/not-permitted')
                }
            },
            {
                path: '/students/create', component: UserEdit, props: { role: 'Student' },
                beforeEnter(to, from, next) {
                    defineAbilitiesFor(store.state.auth.user.role).can('create', 'Students') ? next() : next('/not-permitted')
                }
            },
            {
                path: '/students/:id', component: UserEdit, props: { role: 'Student' },
                beforeEnter(to, from, next) {
                    defineAbilitiesFor(store.state.auth.user.role).can('update', 'Students') ? next() : next('/not-permitted')
                }
            },

            {
                path: '/subjects', component: Subjects,
                beforeEnter(to, from, next) {
                    defineAbilitiesFor(store.state.auth.user.role).can('read', 'Subjects') ? next() : next('/not-permitted')
                }
            },
            {
                path: '/subjects/create', component: SubjectEdit,
                beforeEnter(to, from, next) {
                    defineAbilitiesFor(store.state.auth.user.role).can('create', 'Subjects') ? next() : next('/not-permitted')
                }
            },
            {
                path: '/subjects/:id', component: SubjectEdit,
                beforeEnter(to, from, next) {
                    defineAbilitiesFor(store.state.auth.user.role).can('update', 'Subjects') ? next() : next('/not-permitted')
                }
            },

            {
                path: '/groups', component: Groups,
                beforeEnter(to, from, next) {
                    defineAbilitiesFor(store.state.auth.user.role).can('read', 'Groups') ? next() : next('/not-permitted')
                }
            },
            {
                path: '/groups/create', component: GroupsEdit,
                beforeEnter(to, from, next) {
                    defineAbilitiesFor(store.state.auth.user.role).can('create', 'Groups') ? next() : next('/not-permitted')
                }
            },
            {
                path: '/groups/:id', component: GroupsEdit,
                beforeEnter(to, from, next) {
                    defineAbilitiesFor(store.state.auth.user.role).can('update', 'Groups') ? next() : next('/not-permitted')
                }
            },

            {
                path: '/plans', component: Plans ,
                beforeEnter(to, from, next) {
                    defineAbilitiesFor(store.state.auth.user.role).can('read', 'Plans') ? next() : next('/not-permitted')
                }
            },
            {
                path: '/plans/create', component: PlansEdit ,
                beforeEnter(to, from, next) {
                    defineAbilitiesFor(store.state.auth.user.role).can('create', 'Plans') ? next() : next('/not-permitted')
                }
            },
            {
                path: '/plans/:id', component: PlansEdit ,
                beforeEnter(to, from, next) {
                    defineAbilitiesFor(store.state.auth.user.role).can('update', 'Plans') ? next() : next('/not-permitted')
                }
            },
            {
                path: '/plans/program/:id', component: PlansProgram ,
                beforeEnter(to, from, next) {
                    defineAbilitiesFor(store.state.auth.user.role).can('update', 'PlansProgram') ? next() : next('/not-permitted')
                }
            },

            {
                path: '/lectures', component: Lectures,
                beforeEnter(to, from, next) {
                    defineAbilitiesFor(store.state.auth.user.role).can('read', 'Lectures') ? next() : next('/not-permitted')
                }
            },
            {
                path: '/lectures/create', component: LecturesEdit,
                beforeEnter(to, from, next) {
                    defineAbilitiesFor(store.state.auth.user.role).can('create', 'Lectures') ? next() : next('/not-permitted')
                }
            },
            {
                path: '/lectures/:id', component: LecturesEdit,
                beforeEnter(to, from, next) {
                    defineAbilitiesFor(store.state.auth.user.role).can('update', 'Lectures') ? next() : next('/not-permitted')
                }
            },

            {
                path: '/tests', component: Tests,
                beforeEnter(to, from, next) {
                    defineAbilitiesFor(store.state.auth.user.role).can('read', 'Tests') ? next() : next('/not-permitted')
                }
            },
            {
                path: '/tests/create', component: TestsEdit,
                beforeEnter(to, from, next) {
                    defineAbilitiesFor(store.state.auth.user.role).can('create', 'Tests') ? next() : next('/not-permitted')
                }
            },
            {
                path: '/tests/:id', component: TestsEdit,
                beforeEnter(to, from, next) {
                    defineAbilitiesFor(store.state.auth.user.role).can('update', 'Tests') ? next() : next('/not-permitted')
                }
            },
            {
                path: '/tests/:id/pass', component: TestsPass,
                beforeEnter(to, from, next) {
                    defineAbilitiesFor(store.state.auth.user.role).can('pass', 'Tests') ? next() : next('/not-permitted')
                }
            },
            {
                path: '/marks', component: Marks,
                beforeEnter(to, from, next) {
                    defineAbilitiesFor(store.state.auth.user.role).can('read', 'Tests') ? next() : next('/not-permitted')
                }
            },

            {
                path: '/chats',
                component: Chat ,
                children: [
                    {
                        path: ':id',
                        component: ChatMessages
                    }
                ]
            },
            { path: '/statistic', component: Statistic },
        ],
        meta: { requiresAuth: true }
    },
    { path: '/not-permitted', component: NotPermitted },
    { path: '*', component: NotFound }
];

export const router = new VueRouter({
    routes: routes,
    scrollBehavior (to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition
        } else {
            return { x: 0, y: 0 }
        }
    }
});

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth) && !store.getters.getIsAuthorized) {
        next('/auth')
    } else {
        next()
    }
});
