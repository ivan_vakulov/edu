import Vue from 'vue';
import storage from './storage/storage';
import { abilitiesPlugin } from '@casl/vue';
import defineAbilitiesFor from './abilities/permissions';
import { router } from "./routes/router";
import axios from 'axios';
import {ServerTable, ClientTable, Event} from 'vue-tables-2';

Vue.use(ServerTable);

import * as io from 'socket.io-client';
window.io = io;
import Echo from "laravel-echo";

window.Echo = new Echo({
    broadcaster: 'socket.io',
    host: window.location.hostname + ':6001',
    auth: {
        headers: {
            'Authorization': 'Bearer ' + localStorage.getItem("accessToken")
        }
    }
});

let ability = defineAbilitiesFor(storage.state.auth.user.role);
Vue.use(abilitiesPlugin, ability);

const app = new Vue({
    el: '#app',
    router: router
});

/*
* Set token to axios defaults if it exists
* */
const token = localStorage.getItem('accessToken');
if (token) {
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
} else {
    delete axios.defaults.headers.common['Authorization'];
}

axios.interceptors.response.use(undefined, function (err) {
    return new Promise(function (resolve, reject) {
        if (err.response.status === 401) {
            storage.dispatch('logout').then(() => {
                router.push('/auth')
            });
        } else if (err.response.status === 403) {
            router.push('/not-permitted')
        }
        throw err;
    });
});